This is a template repository for the `geometry` package to be developed in a student project within the
course "Sustainable Development of Simulation Software". For a full description of the project, see
[here][projectreadme]. This template repository predefines a folder structure for the project as well as
a basic [CI](https://docs.gitlab.com/ee/ci/) that automatically executes the tests, lints the code with
[flake8](https://flake8.pycqa.org/en/latest/) and does type checking with [mypy](https://mypy-lang.org/).
To execute these tasks in the command line, move to the top folder of this repository and type:

```bash
python -m pytest
flake8
mypy geometry
```

__Note__: Replace this `README` with an actual description of the project during development.


Requirements
============

As outlined in the [project description][projectreadme], we need to be able to compute geometric properties
of geometric primitives that constitute a computational grid, and we also need to have access to topological
information as e.g. the faces that make up the boundary of a geometric primitive. In particular, we want to
support the following geometric primitives: __segments__, __triangles__, __quadrilaterals__, __tetrahedra__.
We want to represent each of these geometries by a separate _class_, which should

- expose the dimensionality of the geometry and the space dimension it is embedded in (for instance, a triangle in 3d space has a _dimension_ of 2 while the _space dimension_ is 3)
- expose its __length__ (1D) / __area__ (2D) / __volume__ (3D)
- provide access to its __corner points__
- provide access to its __faces__, where each face exposes
    - its unit __outer normal vector__
    - the (primitive-)local indices of the corners that make up the face

Finally, a generic geometry interface (& implementation) should be developed behind which any of the above geometries can be represented.
This allows users of the package to write generic code that uses geometric primitives without having to know the exact geometry types.


Development suggestions
=======================

It is recommended to follow a [TDD](https://en.wikipedia.org/wiki/Test-driven_development)-like development approach.
That is, start with thinking about the next (minimal) development step, and write a test for it __before__ starting the
actual development. This way, you immediately see what the usage of your code looks like and can help you to design
a user-friendly API. As an example, you may want to start the project with implementing a `Point` class to represent a
position in a cartesian coordinate system. The first thing you will need is a way to construct such a class, so you may
start by defining a test for it:

```python
from geometry import Point

def test_point_construction():
    point1D = Point(0.0)
    point2D = Point(0.0, 0.0)
    point3D = Point(0.0, 0.0, 0.0)
```

By writing the test like this, you expect a point to be constructible from a variable number of coordinates, where the
number of passed coordinates determines the space dimension. Alternatively, you could write `Point([0.0, 0.0])` if you
want to construct a point from a _list_ of coordinates or `Point((0.0, 0.0))` for construction from a _tuple_. After
writing the test, it fails because the implementation is missing. Thus, the next task is to provide an implementation
that makes the test pass.


[projectreadme]: https://gitlab.com/sustainable-simulation-software/course-material/-/tree/main/project
